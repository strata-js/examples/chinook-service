# Sqlite Database Files

This folder contains the sqlite database files used by the service. Even if you are not running a sqlite database, 
this folder will contain one file: `chinook_source.db`. This is the original sqlite database file that was used to 
build the migration and seed files. It is included here both for reference, and to allow the seed file to simply 
read out of this database directly when populating the working database.

**DO NOT DELETE `chinook_source.db`!**
