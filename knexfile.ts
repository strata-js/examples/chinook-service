//----------------------------------------------------------------------------------------------------------------------

import 'dotenv/config';
import configUtil from '@strata-js/util-config';
import type { Knex } from 'knex';

import { ServiceConfig } from './src/interfaces/config.js';

//----------------------------------------------------------------------------------------------------------------------

const env = (process.env.ENVIRONMENT ?? 'local').toLowerCase();
configUtil.load(`./config/${ env }.yml`);

//----------------------------------------------------------------------------------------------------------------------

module.exports = {
    ...configUtil.get<ServiceConfig>().database ?? {},
    migrations: {
        directory: './src/knex/migrations',
    },
    seeds: {
        directory: './src/knex/seeds',
    },
} satisfies Knex.Config;

//----------------------------------------------------------------------------------------------------------------------
