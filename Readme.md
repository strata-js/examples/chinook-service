# Chinook Service

This is an example Strata.js service designed to work with the `chinook` example database found here:

* [SqliteTutorial.Net Chinook Example](https://www.sqlitetutorial.net/sqlite-sample-database/)

The origins don't exactly matter, as this is just a sample database around which we've built a service to give an 
example of what taking a traditional relational database and exposing it as a service might look like.

## Database

While the initial data is based off of a sqlite database example, we've converted it to a knex migration and seed 
project, so it can be used with any database supported by knex.

