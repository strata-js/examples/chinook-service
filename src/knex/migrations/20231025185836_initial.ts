//----------------------------------------------------------------------------------------------------------------------
// Initial Setup Migration
//----------------------------------------------------------------------------------------------------------------------

import { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

export async function up(knex : Knex) : Promise<Knex.QueryBuilder>
{
    await knex.schema.createTable('genres', (table) =>
    {
        table.increments('GenreId').primary();
        table.string('Name', 120).notNullable();
    });

    await knex.schema.createTable('media_types', (table) =>
    {
        table.increments('MediaTypeId').primary();
        table.string('Name', 120).notNullable();
    });

    await knex.schema.createTable('artists', (table) =>
    {
        table.increments('ArtistId').primary();
        table.string('Name', 120).notNullable();
    });

    await knex.schema.createTable('albums', (table) =>
    {
        table.increments('AlbumId').primary();
        table.string('Title', 160).notNullable();
        table.integer('ArtistId')
            .references('ArtistId')
            .inTable('artists')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
    });

    await knex.schema.createTable('tracks', (table) =>
    {
        table.increments('TrackId').primary();
        table.string('Name', 200).notNullable();
        table.integer('AlbumId')
            .references('AlbumId')
            .inTable('albums')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
        table.integer('MediaTypeId')
            .references('MediaTypeId')
            .inTable('media_types')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('GenreId')
            .references('GenreId')
            .inTable('genres')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
        table.string('Composer', 220);
        table.integer('Milliseconds').notNullable();
        table.integer('Bytes');
        table.decimal('UnitPrice', 10, 2).notNullable();
    });

    await knex.schema.createTable('playlists', (table) =>
    {
        table.increments('PlaylistId').primary();
        table.string('Name', 120).notNullable();
    });

    await knex.schema.createTable('playlist_track', (table) =>
    {
        table.integer('PlaylistId')
            .references('PlaylistId')
            .inTable('playlists')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('TrackId')
            .references('TrackId')
            .inTable('tracks')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');

        // Mark them as a composite key
        table.primary([ 'PlaylistId', 'TrackId' ]);
    });

    await knex.schema.createTable('employees', (table) =>
    {
        table.increments('EmployeeId').primary();
        table.string('LastName', 20).notNullable();
        table.string('FirstName', 20).notNullable();
        table.string('Title', 30);
        table.integer('ReportsTo')
            .references('EmployeeId')
            .inTable('employees')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
        table.date('BirthDate');
        table.date('HireDate');
        table.string('Address', 70);
        table.string('City', 40);
        table.string('State', 40);
        table.string('Country', 40);
        table.string('PostalCode', 10);
        table.string('Phone', 24);
        table.string('Fax', 24);
        table.string('Email', 60);
    });

    await knex.schema.createTable('customers', (table) =>
    {
        table.increments('CustomerId').primary();
        table.string('FirstName', 40).notNullable();
        table.string('LastName', 20).notNullable();
        table.string('Company', 80);
        table.string('Address', 70);
        table.string('City', 40);
        table.string('State', 40);
        table.string('Country', 40);
        table.string('PostalCode', 10);
        table.string('Phone', 24);
        table.string('Fax', 24);
        table.string('Email', 60).notNullable();
        table.integer('SupportRepId')
            .references('EmployeeId')
            .inTable('employees')
            .onUpdate('CASCADE')
            .onDelete('SET NULL');
    });

    await knex.schema.createTable('invoices', (table) =>
    {
        table.increments('InvoiceId').primary();
        table.integer('CustomerId')
            .references('CustomerId')
            .inTable('customers')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.date('InvoiceDate').notNullable();
        table.string('BillingAddress', 70);
        table.string('BillingCity', 40);
        table.string('BillingState', 40);
        table.string('BillingCountry', 40);
        table.string('BillingPostalCode', 10);
        table.decimal('Total', 10, 2).notNullable();
    });

    await knex.schema.createTable('invoice_items', (table) =>
    {
        table.increments('InvoiceLineId').primary();
        table.integer('InvoiceId')
            .references('InvoiceId')
            .inTable('invoices')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.integer('TrackId')
            .references('TrackId')
            .inTable('tracks')
            .notNullable()
            .onUpdate('CASCADE')
            .onDelete('CASCADE');
        table.decimal('UnitPrice', 10, 2).notNullable();
        table.integer('Quantity').notNullable();
    });
}

//----------------------------------------------------------------------------------------------------------------------

export async function down(knex : Knex) : Promise<Knex.QueryBuilder>
{
    await knex.schema.dropTable('albums');
    await knex.schema.dropTable('artists');
    await knex.schema.dropTable('customers');
    await knex.schema.dropTable('employees');
    await knex.schema.dropTable('genres');
    await knex.schema.dropTable('invoice_items');
    await knex.schema.dropTable('invoices');
    await knex.schema.dropTable('media_types');
    await knex.schema.dropTable('playlist_track');
    await knex.schema.dropTable('playlists');
    await knex.schema.dropTable('tracks');
}

//----------------------------------------------------------------------------------------------------------------------

