//----------------------------------------------------------------------------------------------------------------------
// Initial Sample Data Seed
//----------------------------------------------------------------------------------------------------------------------

import db, { Knex } from 'knex';

//----------------------------------------------------------------------------------------------------------------------

/* eslint-disable no-await-in-loop */

export async function seed(knex : Knex) : Promise<void>
{
    // Make a new knex connection to the source sqlite database
    const sourceDB = db({
        client: 'better-sqlite3',
        connection: {
            filename: './db/chinook_source.db'
        },
        useNullAsDefault: true
    });

    // We could get the tables from the source database, but we have to insert them in a specific order, due to the
    // foreign keys. So, we'll just hard-code the tables here.
    // const tables = await sourceDB('sqlite_master')
    //     .select('name')
    //     .where({ type: 'table' })
    //     .andWhere('name', 'NOT LIKE', 'sqlite_%')
    //     .andWhere('name', 'NOT LIKE', 'knex_%')
    //     .orderBy('name');

    const tables = [
        'genres',
        'media_types',
        'artists',
        'albums',
        'tracks',
        'playlists',
        'playlist_track',
        'employees',
        'customers',
        'invoices',
        'invoice_items'
    ];

    // Loop through the tables, and copy them to the target database
    for(const table of tables)
    {
        const tableName = table;
        const tableData = await sourceDB(tableName).select('*');

        // Batch insert the data into the target database only 1000 rows at a time
        const limit = 100;
        for(let i = 0; i < tableData.length; i += limit)
        {
            await knex(tableName).insert(tableData.slice(i, i + limit));
        }
    }

    // Close the source database connection
    await sourceDB.destroy();
}

//----------------------------------------------------------------------------------------------------------------------
