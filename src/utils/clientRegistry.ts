//----------------------------------------------------------------------------------------------------------------------
// A map of Strata Clients
//----------------------------------------------------------------------------------------------------------------------

import { StrataClient, StrataClientConfig } from '@strata-js/strata';
import configUtil from '@strata-js/util-config';

//----------------------------------------------------------------------------------------------------------------------

const clients = new Map<string, StrataClient>();

export function registerClient(name : string, client : StrataClient) : void
{
    clients.set(name, client);
}

export function getClient(name : string) : StrataClient | undefined
{
    return clients.get(name);
}

export async function getOrCreateClient(name : string) : Promise<StrataClient>
{
    const config = configUtil.get<StrataClientConfig>();

    let client = clients.get(name);
    if(!client)
    {
        client = new StrataClient({
            ...config,
            client: { name },
        });

        // Start the client
        await client.start();

        clients.set(name, client);
    }

    return client;
}

export function getClientNames() : string[]
{
    return Array.from(clients.keys());
}

export function listClients() : StrataClient[]
{
    return Array.from(clients.values());
}

export function clearClients() : void
{
    clients.clear();
}

//----------------------------------------------------------------------------------------------------------------------
