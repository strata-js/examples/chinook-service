//----------------------------------------------------------------------------------------------------------------------
// entity.ts
//----------------------------------------------------------------------------------------------------------------------

import { Employee } from '../../models/entity.js';

//----------------------------------------------------------------------------------------------------------------------

export interface EmployeeGetRequest
{
    id : string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface EmployeeListRequest
{
}

export interface EmployeeCreateRequest extends Omit<Employee, 'id' | 'hireDate' | 'birthDate'>
{
    hireDate : string;
    birthDate : string;
}

export interface EmployeeUpdateRequest extends Partial<Omit<Employee, 'id' | 'hireDate' | 'birthDate'>>
{
    id : string;
    hireDate ?: string;
    birthDate ?: string;
}

//----------------------------------------------------------------------------------------------------------------------
