// ---------------------------------------------------------------------------------------------------------------------
// Chinook Service
// ---------------------------------------------------------------------------------------------------------------------

import 'dotenv/config';
import { hostname } from 'node:os';
import { StrataService, logging } from '@strata-js/strata';
import configUtil from '@strata-js/util-config';

// Interfaces
import { ServiceConfig } from './interfaces/config.js';

// Contexts
import testContext from './contexts/test.js';
import entityContext from './contexts/entity.js';

// ---------------------------------------------------------------------------------------------------------------------

const logger = logging.getLogger('service');

// ---------------------------------------------------------------------------------------------------------------------

// Determine environment
const env = process.env.ENVIRONMENT ?? 'local';
process.env.HOSTNAME = process.env.HOSTNAME ?? hostname();

// Load config
configUtil.load(`./config/${ env }.yml`);

// Set Logging config
logging.setConfig(configUtil.get<ServiceConfig>()?.logging ?? {});

// Create the service instance
const service = new StrataService(configUtil.get());

// Register Contexts
service.registerContext('test', testContext);
service.registerContext('entity', entityContext);

// Start the service
async function main() : Promise<void>
{
    // Start the service
    await service.start();
}

// ---------------------------------------------------------------------------------------------------------------------

main()
    .catch((error) =>
    {
        logger.error(`Encountered unhandled exception. Exiting.`, error.stack);
    });

// ---------------------------------------------------------------------------------------------------------------------
