//----------------------------------------------------------------------------------------------------------------------
// Entity Context
//----------------------------------------------------------------------------------------------------------------------

import { Context } from '@strata-js/strata';
import { PayloadValidationMiddleware } from '@strata-js/middleware-payload-validation';

// Managers
import entityManager from '../managers/entity.js';

// Validation
import { employeePayloadSchema } from '../contexts/validations/entity.js';

// ---------------------------------------------------------------------------------------------------------------------

const context = new Context();
const entityPayloadValidationMiddleware = new PayloadValidationMiddleware(employeePayloadSchema);

context.registerOperationsForInstance(entityManager, [ 'payload' ], [ entityPayloadValidationMiddleware ]);

export default context;

//----------------------------------------------------------------------------------------------------------------------
