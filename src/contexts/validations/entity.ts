//----------------------------------------------------------------------------------------------------------------------
// Entity Payload Validation
//----------------------------------------------------------------------------------------------------------------------

import { JSONSchemaType } from 'ajv';

// Requests
import * as Requests from '../../interfaces/requests/index.js';

//----------------------------------------------------------------------------------------------------------------------

const EntityGetEmployeePayloadSchema : JSONSchemaType<Requests.EmployeeGetRequest> = {
    type: 'object',
    properties: {
        id: { type: 'string' },
    },
    required: [ 'id' ],
    additionalProperties: false,
};

const EntityListEmployeesPayloadSchema : JSONSchemaType<Requests.EmployeeListRequest> = {
    type: 'object',
    properties: {
    },
    required: [],
    additionalProperties: false,
};

const EntityCreateEmployeePayloadSchema : JSONSchemaType<Requests.EmployeeCreateRequest> = {
    type: 'object',
    properties: {
        firstName: { type: 'string', maxLength: 20 },
        lastName: { type: 'string', maxLength: 20 },
        title: { type: 'string', maxLength: 30 },
        reportsTo: { type: 'string' },
        birthDate: { type: 'string', maxLength: 10 },
        hireDate: { type: 'string', maxLength: 10 },
        address: { type: 'string', maxLength: 70 },
        city: { type: 'string', maxLength: 40 },
        state: { type: 'string', maxLength: 40 },
        country: { type: 'string', maxLength: 40 },
        postalCode: { type: 'string', maxLength: 10 },
        phone: { type: 'string', maxLength: 24 },
        fax: { type: 'string', maxLength: 24 },
        email: { type: 'string', maxLength: 60 },
    },
    required: [
        'firstName',
        'lastName',
        'title',
        'reportsTo',
        'birthDate',
        'hireDate',
        'address',
        'city',
        'state',
        'country',
        'postalCode',
        'phone',
        'fax',
        'email',
    ],
    additionalProperties: false,
};

const EntityUpdateEmployeePayloadSchema : JSONSchemaType<Requests.EmployeeUpdateRequest> = {
    type: 'object',
    properties: {
        id: { type: 'string' },
        firstName: { type: 'string', maxLength: 20, nullable: true },
        lastName: { type: 'string', maxLength: 20, nullable: true },
        title: { type: 'string', maxLength: 30, nullable: true },
        reportsTo: { type: 'string', nullable: true },
        birthDate: { type: 'string', maxLength: 10, nullable: true },
        hireDate: { type: 'string', maxLength: 10, nullable: true },
        address: { type: 'string', maxLength: 70, nullable: true },
        city: { type: 'string', maxLength: 40, nullable: true },
        state: { type: 'string', maxLength: 40, nullable: true },
        country: { type: 'string', maxLength: 40, nullable: true },
        postalCode: { type: 'string', maxLength: 10, nullable: true },
        phone: { type: 'string', maxLength: 24, nullable: true },
        fax: { type: 'string', maxLength: 24, nullable: true },
        email: { type: 'string', maxLength: 60, nullable: true },
    },
    required: [ 'id' ],
    additionalProperties: false,
};

//----------------------------------------------------------------------------------------------------------------------

export const employeePayloadSchema = {
    getEmployee: EntityGetEmployeePayloadSchema,
    listEmployees: EntityListEmployeesPayloadSchema,
    createEmployee: EntityCreateEmployeePayloadSchema,
    updateEmployee: EntityUpdateEmployeePayloadSchema,
};

//----------------------------------------------------------------------------------------------------------------------
