//----------------------------------------------------------------------------------------------------------------------
// Invoice Models
//----------------------------------------------------------------------------------------------------------------------

type TrackID = string;
type InvoiceID = string;
type InvoiceItemID = string;
type CustomerID = string;

export interface InvoiceItem
{
    id : InvoiceItemID;
    track : TrackID;
    quantity : number;
    price : number;
}

export interface Invoice
{
    id : InvoiceID;
    customer : CustomerID;
    date : Date;
    billingAddress : string;
    billingCity : string;
    billingState : string;
    billingCountry : string;
    billingPostalCode : string;
    items : InvoiceItem[];
    total : number;
}

//----------------------------------------------------------------------------------------------------------------------
