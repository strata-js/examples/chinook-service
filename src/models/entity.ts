//----------------------------------------------------------------------------------------------------------------------
// Entity Models
//----------------------------------------------------------------------------------------------------------------------

type EmployeeID = string;

export interface Employee
{
    id : EmployeeID;
    firstName : string;
    lastName : string;
    title : string;
    reportsTo : EmployeeID | null;
    birthDate : Date;
    hireDate : Date;
    address : string;
    city : string;
    state : string;
    country : string;
    postalCode : string;
    phone : string;
    fax : string;
    email : string;
}

//----------------------------------------------------------------------------------------------------------------------

type CustomerID = string;

export interface Customer
{
    id : CustomerID;
    firstName : string;
    lastName : string;
    company : string;
    address : string;
    city : string;
    state : string;
    country : string;
    postalCode : string;
    phone : string;
    fax : string;
    email : string;
    supportRepID : EmployeeID;
}

//----------------------------------------------------------------------------------------------------------------------

type ArtistID = string;

export interface Artist
{
    id : ArtistID;
    name : string;
}

//----------------------------------------------------------------------------------------------------------------------
