//----------------------------------------------------------------------------------------------------------------------
// Music Models
//----------------------------------------------------------------------------------------------------------------------

type MediaTypeID = string;
export interface MediaType
{
    id : MediaTypeID;
    name : string;
}

type GenreID = string;
export interface Genre
{
    id : GenreID;
    name : string;
}

type ArtistID = string;
export interface Artist
{
    id : ArtistID;
    name : string;
}

type AlbumID = string;
export interface Album
{
    id : AlbumID;
    title : string;
    artist : Artist;
}

type TrackID = string;
export interface Track
{
    id : TrackID;
    name : string;
    album : Album;
    mediaType : MediaType;
    genre : Genre;
    composer : string;
    milliseconds : number;
    bytes : number;
    unitPrice : number;
}

type PlaylistID = string;
export interface Playlist
{
    id : PlaylistID;
    name : string;
    tracks : Track[];
}

//----------------------------------------------------------------------------------------------------------------------
