//----------------------------------------------------------------------------------------------------------------------
// Errors
//----------------------------------------------------------------------------------------------------------------------

import { errors as StrataErrors } from '@strata-js/strata';

//----------------------------------------------------------------------------------------------------------------------

export const ServiceError = StrataErrors.ServiceError;

//----------------------------------------------------------------------------------------------------------------------

export class ExtendedServiceError extends StrataErrors.ServiceError
{
    public code = 'EXTENDED_SERVICE_ERROR';

    constructor(message : string)
    {
        super(message);
    }
}
//----------------------------------------------------------------------------------------------------------------------

export class ExtendedError extends Error
{
    public code = 'EXTENDED_ERROR';
    constructor(message : string)
    {
        super(message);
    }
}
//----------------------------------------------------------------------------------------------------------------------

export class ExtendedErrorSafe extends Error
{
    public code = 'EXTENDED_ERROR_SAFE';
    public isSafeMessage = true;
    constructor(message : string)
    {
        super(message);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export class NotFoundError extends ExtendedServiceError
{
    public code = 'NOT_FOUND';
    constructor(message : string)
    {
        super(message);
    }
}

//----------------------------------------------------------------------------------------------------------------------

export class ValidationError extends ServiceError
{
    public code = 'VALIDATION';

    constructor(message : string)
    {
        super(message);
    }
}

//----------------------------------------------------------------------------------------------------------------------
