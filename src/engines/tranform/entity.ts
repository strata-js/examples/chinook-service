//----------------------------------------------------------------------------------------------------------------------
// Entity Transformation Engine
//----------------------------------------------------------------------------------------------------------------------

import { Employee } from '../../models/entity.js';
import * as Requests from '../../interfaces/requests/index.js';

//----------------------------------------------------------------------------------------------------------------------

export function createEmployeeFromCreateRequest(request : Requests.EmployeeCreateRequest) : Omit<Employee, 'id'>
{
    return {
        firstName: request.firstName,
        lastName: request.lastName,
        title: request.title,
        reportsTo: request.reportsTo,
        birthDate: new Date(request.birthDate),
        hireDate: new Date(request.hireDate),
        address: request.address,
        city: request.city,
        state: request.state,
        country: request.country,
        postalCode: request.postalCode,
        phone: request.phone,
        fax: request.fax,
        email: request.email,
    };
}

export function createEmployeeFromUpdateRequest(request : Requests.EmployeeUpdateRequest) : Partial<Employee>
{
    return {
        id: request.id,
        firstName: request.firstName,
        lastName: request.lastName,
        title: request.title,
        reportsTo: request.reportsTo,
        birthDate: request.birthDate ? new Date(request.birthDate) : undefined,
        hireDate: request.hireDate ? new Date(request.hireDate) : undefined,
        address: request.address,
        city: request.city,
        state: request.state,
        country: request.country,
        postalCode: request.postalCode,
        phone: request.phone,
        fax: request.fax,
        email: request.email,
    };
}

//----------------------------------------------------------------------------------------------------------------------

export default {
    createEmployeeFromCreateRequest,
    createEmployeeFromUpdateRequest,
};

//----------------------------------------------------------------------------------------------------------------------
