//----------------------------------------------------------------------------------------------------------------------
// Entity Validation Engine
//----------------------------------------------------------------------------------------------------------------------

import { NotFoundError, ValidationError } from '../../errors.js';

// Models
import { Employee } from '../../models/entity.js';

// Resource Access
import employeeRA from '../../resource-access/employee.js';

//----------------------------------------------------------------------------------------------------------------------

export async function validateEmployee(employee : Partial<Employee>) : Promise<void>
{
    // Ensure that the employee's reportsTo exists, or is null
    if(employee.reportsTo !== undefined)
    {
        if(employee.reportsTo !== null)
        {
            try
            {
                await employeeRA.get(parseInt(employee.reportsTo, 10));
            }
            catch (error)
            {
                if(error instanceof NotFoundError)
                {
                    throw new ValidationError(
                        `reportsTo: Employee with id '${ employee.reportsTo }' does not exist`
                    );
                }
                else
                {
                    throw error;
                }
            }
        }
    }

    // Ensure that the employee's birthDate is over the age of 18
    if(employee.birthDate !== undefined)
    {
        const birthDate = new Date(employee.birthDate);
        const now = new Date();
        const age = now.getFullYear() - birthDate.getFullYear();
        if(age < 18)
        {
            throw new ValidationError('birthDate: Employee must be over the age of 18');
        }
    }

    // Ensure that the employee's hireDate is not in the future
    if(employee.hireDate !== undefined)
    {
        const hireDate = new Date(employee.hireDate);
        const now = new Date();
        if(hireDate > now)
        {
            throw new ValidationError('hireDate: Employee cannot be hired in the future');
        }
    }

    // Ensure that the employee's hireDate is not before their birthDate
    if(employee.id || (employee.birthDate !== undefined && employee.hireDate !== undefined))
    {
        let hireDate : Date;
        let birthDate : Date;

        if(employee.id)
        {
            const employeeRecord = await employeeRA.get(parseInt(employee.id, 10));
            hireDate = employee.hireDate ? new Date(employee.hireDate) : new Date(employeeRecord.hireDate);
            birthDate = employee.birthDate ? new Date(employee.birthDate) : new Date(employeeRecord.birthDate);
        }
        else
        {
            hireDate = employee.hireDate as Date;
            birthDate = employee.birthDate as Date;
        }

        if(hireDate < birthDate)
        {
            throw new ValidationError('hireDate: Employee cannot be hired before they are born');
        }
    }

    // Ensure that the employee's hireDate is after they turned 18
    if(employee.birthDate !== undefined || employee.hireDate !== undefined)
    {
        const birthDate = employee.birthDate ? new Date(employee.birthDate) : undefined;
        const hireDate = employee.hireDate ? new Date(employee.hireDate) : undefined;
        if(birthDate && hireDate)
        {
            const eighteenYears = 18 * 365 * 24 * 60 * 60 * 1000;
            if(hireDate.getTime() - birthDate.getTime() < eighteenYears)
            {
                throw new ValidationError('hireDate: Employee cannot be hired before they turned 18');
            }
        }
    }

    // Ensure that the employee's postalCode is a valid US postal code
    if(employee.postalCode !== undefined)
    {
        const postalCode = employee.postalCode;
        if(!postalCode.match(/^\d{5}(-\d{4})?$/))
        {
            throw new ValidationError('postalCode: Employee postal code must be a valid US postal code');
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------

export default {
    validateEmployee,
};

//----------------------------------------------------------------------------------------------------------------------
