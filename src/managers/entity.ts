//----------------------------------------------------------------------------------------------------------------------
// Entity Manager
//----------------------------------------------------------------------------------------------------------------------

// Models
import { Employee } from '../models/entity.js';
import * as Requests from '../interfaces/requests/index.js';

// Engines
import entityTE from '../engines/tranform/entity.js';
import entityVE from '../engines/validation/entity.js';

// Resource Access
import employeeRA from '../resource-access/employee.js';

//----------------------------------------------------------------------------------------------------------------------

export async function getEmployee(request : Requests.EmployeeGetRequest) : Promise<Employee>
{
    return employeeRA.get(parseInt(request.id, 10));
}

export async function listEmployees() : Promise<Employee[]>
{
    return employeeRA.list();
}

export async function createEmployee(request : Requests.EmployeeCreateRequest) : Promise<Employee>
{
    // Create an employee (minus 'id') from the request
    const employee = entityTE.createEmployeeFromCreateRequest(request);

    // Ensure the employee is valid
    await entityVE.validateEmployee(employee);

    // Create the employee
    return employeeRA.create(employee);
}

export async function updateEmployee(request : Requests.EmployeeUpdateRequest) : Promise<Employee>
{
    // Create a partial employee record from the request
    const employee = entityTE.createEmployeeFromUpdateRequest(request);

    // Ensure the employee is valid
    await entityVE.validateEmployee(employee);

    // Update the employee
    return employeeRA.update(employee);
}

//----------------------------------------------------------------------------------------------------------------------

export default {
    getEmployee,
    listEmployees,
    createEmployee,
    updateEmployee,
};

//----------------------------------------------------------------------------------------------------------------------
