//----------------------------------------------------------------------------------------------------------------------
// Employee Resource Access
//----------------------------------------------------------------------------------------------------------------------

// Models
import { Employee } from '../models/entity.js';

// Utils
import { getDB } from '../utils/database.js';
import { NotFoundError } from '../errors.js';

//----------------------------------------------------------------------------------------------------------------------

interface EmployeeRecord
{
    EmployeeId : number;
    LastName : string;
    FirstName : string;
    Title : string;
    ReportsTo : number | null;
    BirthDate : string;
    HireDate : string;
    Address : string;
    City : string;
    State : string;
    Country : string;
    PostalCode : string;
    Phone : string;
    Fax : string;
    Email : string;
}

//----------------------------------------------------------------------------------------------------------------------

function _fromDB(row : EmployeeRecord) : Employee
{
    return {
        id: `${ row.EmployeeId }`,
        lastName: row.LastName,
        firstName: row.FirstName,
        title: row.Title,
        reportsTo: `${ row.ReportsTo }`,
        birthDate: new Date(row.BirthDate),
        hireDate: new Date(row.HireDate),
        address: row.Address,
        city: row.City,
        state: row.State,
        country: row.Country,
        postalCode: row.PostalCode,
        phone: row.Phone,
        fax: row.Fax,
        email: row.Email,
    };
}

function _toDB(employee : Partial<Employee>) : Partial<EmployeeRecord>
{
    const employeeID = employee.id ? parseInt(employee.id, 10) : undefined;
    const reportsTo = employee.reportsTo ? parseInt(employee.reportsTo, 10) : undefined;

    return {
        EmployeeId: employeeID,
        LastName: employee.lastName,
        FirstName: employee.firstName,
        Title: employee.title,
        ReportsTo: reportsTo,
        BirthDate: employee.birthDate?.toISOString(),
        HireDate: employee.hireDate?.toISOString(),
        Address: employee.address,
        City: employee.city,
        State: employee.state,
        Country: employee.country,
        PostalCode: employee.postalCode,
        Phone: employee.phone,
        Fax: employee.fax,
        Email: employee.email,
    };
}

export async function get(id : number) : Promise<Employee>
{
    const db = await getDB();
    const rows = await db<EmployeeRecord>('employees').select()
        .where('EmployeeId', id);

    if(rows.length === 0)
    {
        throw new NotFoundError(`No employee with id ${ id } found.`);
    }

    return _fromDB(rows[0]);
}

export async function list() : Promise<Employee[]>
{
    const db = await getDB();
    const rows = await db<EmployeeRecord>('employees').select();
    return rows.map(_fromDB);
}

export async function create(employee : Omit<Employee, 'id'>) : Promise<Employee>
{
    const db = await getDB();
    const row = _toDB(employee);
    const [ { EmployeeId } ] = await db<EmployeeRecord>('employees').insert(row)
        .returning('EmployeeId');
    return get(EmployeeId);
}

export async function update(employee : Partial<Employee>) : Promise<Employee>
{
    const db = await getDB();
    const row = _toDB(employee);
    if(!row.EmployeeId)
    {
        throw new Error('Employee ID is required to update an employee.');
    }

    await db<EmployeeRecord>('employees').update(row)
        .where('EmployeeId', row.EmployeeId);
    return get(row.EmployeeId);
}

//----------------------------------------------------------------------------------------------------------------------

export default {
    get,
    list,
    create,
    update,
};

//----------------------------------------------------------------------------------------------------------------------
